# Do not try this at home

Seriously, don't.

If you run this on FreeBSD, you need to do:

```console
sysctl security.bsd.map_at_zero=1
```
