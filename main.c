/*
 * -----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * nsonack@outlook.com wrote this file.  As long as you retain this
 * notice you can do whatever you want with this stuff. If we meet
 * some day, and you think this stuff is worth it, you can buy me a
 * beer in return.  Nico Sonack
 * -----------------------------------------------------------------------------
 *
 *    Map a file of machine code into executable memory and jump into it.
 *
 *       Associated machine code can be found in vector.c
 */

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

int
main(void)
{
    int fd = open("vector.bin", O_RDONLY);
    if (fd < 0)
        err(1, "open");

    struct stat sb = {0};
    if (fstat(fd, &sb) < 0)
        err(1, "fstat");

    if (setuid(0) < 0)
        err(1, "setuid");

    void *page = mmap(NULL, sb.st_size, PROT_READ | PROT_EXEC,
                      MAP_PRIVATE|MAP_FIXED, fd, 0);
    if (page == MAP_FAILED)
        err(1, "mmap");

    int (*fn)(void) = NULL;
    int x = fn();

    printf("%d\n", x);

    return 0;
}
