/*
 * -----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * nsonack@outlook.com wrote this file.  As long as you retain this
 * notice you can do whatever you want with this stuff. If we meet
 * some day, and you think this stuff is worth it, you can buy me a
 * beer in return.  Nico Sonack
 * -----------------------------------------------------------------------------
 *
 *    Map a file of machine code into executable memory and jump into it.
 *
 *       a boilerplate piece of code of a function that returns 42
 */

int
random(void)
{
    return 42;
}
