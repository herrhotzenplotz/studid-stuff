CFLAGS=-O0 -pipe -fno-fast-math -std=c99
.PHONY: clean all setuid
all: main vector.bin setuid

main: main.c
	${CC} ${CFLAGS} -o main main.c

vector.bin: vector.c
	${CC} ${CFLAGS} -c -o vector.o vector.c
	objcopy -O binary -K random --only-section=.text vector.o vector.bin

setuid: main
	su root -c '/bin/sh -c "chown root:wheel main && chmod +s main"'

clean:
	rm -f vector.o main vector.bin
